# Desafio Webjump
Teste para vaga de desenvolvedor frontend na Webjump, tornar o layout proposto em uma página funcional.

![Layout](assets/preview.jpg)

## Instruções para rodar a aplicacão

Você pode acessar pelo deploy BETA em http://webjump-rickyalmeida.herokuapp.com/. Essa versão pode estar instável devido a API mocada. Portanto recomendo que siga os passos abaixo:

***Ambiente de desenvolvimento***

- Primeiro clone o projeto para sua máquina rodando o seguinte comando no terminal:

```
git clone https://rickyalmeidadev@bitbucket.org/rickyalmeidadev/assessment-frontend.git
```

- Na raiz do projeto, crie um aquivo .env e coloque nele as variáveis de ambiente:

```
PORT=8888
```

- Ainda na raiz, rode os seguintes comandos no terminal:

```
npm install
npm start
```

- Agora caminhe até o diretório client e crie um arquivo .env.development.local e coloque nele as variáveis de ambiente:

```
REACT_APP_URL=http://localhost:8888/api/V1/categories
```

- No terminal, dentro do diretório client use os seguintes comandos:

```
npm install
npm start
```

Pronto, a aplicação será aberta em seu navegador padrão, ou vou pode acessar em http://localhost:3000.

## Tecnologias usadas

- [React](https://reactjs.org/)
- [React Hooks](https://pt-br.reactjs.org/docs/hooks-intro.html)
- [React Router](https://github.com/ReactTraining/react-router)
- [Axios](https://github.com/axios/axios)
- [SASS](https://github.com/sass/sass)

## Requisitos
- [x] Design responsivo nos breakpoints 320px, 768px, 1024px e 1440px
- [x] Suporte para IE, Chrome, Safari, Firefox
- [x] Semântica

## Observações

- Os links de navegação são gerados de forma dinâmica, baseado na lista da api.
- Os filtros do sidebar estão funcionais.
- Decidi criar mais um breakpoint para deixar o grid com apenas uma coluna quando igual ou menor que 320px de largura, pois acredito que para celulares muito pequenos a visualização em duas colunas pode ser difícil.

## Autor

Feito com :heart: por Ricky Almeida.
