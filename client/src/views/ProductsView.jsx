import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import ProductGrid from '../components/ProductGrid';
import Sidebar from '../components/Sidebar';
import api from '../services/api';

const ProductsView = ({ id, name }) => {
  const [items, setItems] = useState([]);
  const [filterOptions, setFilterOptions] = useState(null);
  const [filters, setFilters] = useState([]);
  const [sortType, setSortType] = useState('');
  const [selectedOptions, setSelectedOptions] = useState([]);

  useEffect(() => {
    api.get(id).then(response => {
      const { filters, items } = response.data;
      setItems(items);
      setFilterOptions(filters[0]);

      const filtersValues = items.map(item => Object.values(item.filter[0])[0]);
      const removedDuplicates = [...new Set(filtersValues)];

      setFilters(removedDuplicates);
      setSelectedOptions([]);
      setSortType('');
    });
  }, [id]);

  useEffect(() => {
    if (sortType === 'name') {
      const sorted = [...items].sort((a, b) => a.name.localeCompare(b.name));
      setItems(sorted);
    }

    if (sortType === 'price') {
      const sorted = [...items].sort((a, b) => {
        if (a.specialPrice && b.specialPrice) {
          return a.specialPrice - b.specialPrice;
        } else if (b.specialPrice) {
          return a.price - b.specialPrice;
        } else if (a.specialPrice) {
          return a.specialPrice - b.price;
        } else {
          return a.price - b.price;
        }
      });
      setItems(sorted);
    }
    // eslint-disable-next-line
  }, [sortType]);

  const handleSort = event => {
    const { value } = event.target;
    setSortType(value);
  };

  const handleFilters = event => {
    // This id is the name of the filter option
    const { id } = event.target;
    const index = selectedOptions.findIndex(option => option === id);

    if (index >= 0) {
      const copyArr = [...selectedOptions];
      copyArr.splice(index, 1);

      setSelectedOptions(copyArr);
    } else {
      setSelectedOptions([...selectedOptions, id]);
    }
  };

  const handleClear = () => {
    setSelectedOptions([]);
  };

  if (!items.length || !filterOptions) {
    return (
      <div className="loading">
        <BounceLoader color="#cc0c1f" />
      </div>
    );
  }

  return (
    <main className="wrapper wrapper--main">
      <div className="container">
        <p className="path">
          <Link to="/">Página inicial</Link> &gt;
          <span className="path--active"> {name}</span>
        </p>
      </div>
      <div className="container container--content">
        <Sidebar
          filterOptions={filterOptions}
          filters={filters}
          handleFilters={handleFilters}
          selectedOptions={selectedOptions}
          handleClear={handleClear}
        />
        <section className="products">
          <h1 className="products__title">{name}</h1>
          <hr />
          <div className="products__controls">
            <div className="products__controls__sort">
              <span>Ordernar por</span>{' '}
              <select
                name="sort"
                id="sort"
                value={sortType}
                onChange={handleSort}
              >
                <option value="">Escolher</option>
                <option value="name">Nome</option>
                <option value="price">Preço</option>
              </select>
            </div>
          </div>
          <hr />
          <ProductGrid
            items={items.filter(item =>
              selectedOptions.length
                ? selectedOptions.includes(Object.values(item.filter[0])[0])
                : true,
            )}
          />
        </section>
      </div>
    </main>
  );
};

export default ProductsView;
