import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import api from '../services/api';

const Home = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    api.get('list').then(response => {
      setCategories(response.data.items);
    });
  }, []);

  if (!categories.length) {
    return (
      <div className="loading">
        <BounceLoader color="#cc0c1f" />
      </div>
    );
  }

  return (
    <main className="wrapper">
      <div className="container">
        <section className="home">
          <h1 className="home__title">Escolha uma categoria:</h1>
          <div className="home__controls">
            {categories.map(category => (
              <Link key={category.id} to={category.path}>
                <button className="btn btn--home">{category.name}</button>
              </Link>
            ))}
          </div>
        </section>
      </div>
    </main>
  );
};

export default Home;
