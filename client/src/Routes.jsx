import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ProductsView from './views/ProductsView';
import Home from './views/Home';

const Routes = () => {
  return (
    <Switch>
      <Route
        exact
        path="/"
        render={props => <Home {...props} id="all" />}
      />
      <Route
        exact
        path="/camisetas"
        render={props => <ProductsView {...props} id="1" name="Camisetas" />}
      />
      <Route
        exact
        path="/calcas"
        render={props => <ProductsView {...props} id="2" name="Calças" />}
      />
      <Route
        exact
        path="/calcados"
        render={props => <ProductsView {...props} id="3" name="Calçados" />}
      />
    </Switch>
  );
};

export default Routes;
