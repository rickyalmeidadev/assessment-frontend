import React from 'react';

const SearchBar = () => (
  <div className="search__bar">
    <input className="input input--search" type="text" />
    <button className=" btn btn--search">Buscar</button>
  </div>
);
export default SearchBar;
