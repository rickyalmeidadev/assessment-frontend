import React from 'react';

const Sidebar = ({
  filterOptions,
  filters,
  handleFilters,
  selectedOptions,
  handleClear,
}) => {
  return (
    <aside className="filter-options">
      <h2 className="filter-options__title">Filtre por</h2>
      <h3 className="filter-options__type">{Object.values(filterOptions)}</h3>
      <ul className="filter-options__list">
        {filters.map(option => (
          <li
            key={option}
            id={option}
            className={`filter-options__item ${
              selectedOptions.includes(option)
                ? 'filter-options__item--active'
                : ''
            }`}
            onClick={handleFilters}
          >
            {option}
          </li>
        ))}
      </ul>
      {selectedOptions.length ? (
        <span className="filter-options__clear" onClick={handleClear}>
          Remover filtros
        </span>
      ) : null}
    </aside>
  );
};

export default Sidebar;
