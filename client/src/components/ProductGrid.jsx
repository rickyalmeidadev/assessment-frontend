import React from 'react';
import './ProductCard';
import ProductCard from './ProductCard';

const ProductGrid = ({ items }) => {
  return (
    <section className="grid">
      {items.map(item => (
        <ProductCard key={item.id} {...item} />
      ))}
    </section>
  );
};

export default ProductGrid;
