import React from 'react';

const ProductCard = ({ name, image, price, specialPrice, path }) => {
  return (
    <article className="product">
      <figure className="product__img">
        <img src={image} alt={name} />
      </figure>
      <h2 className="product__title">{name}</h2>
      {specialPrice ? (
        <div className="product__price">
          <span className="product__price--inactive">
            R${price.toFixed(2).replace('.', ',')}
          </span>
          <span>R${specialPrice.toFixed(2).replace('.', ',')}</span>
        </div>
      ) : (
        <p className="product__price">R${price.toFixed(2).replace('.', ',')}</p>
      )}
      <button className="btn btn--buy"> Comprar</button>
    </article>
  );
};

export default ProductCard;
