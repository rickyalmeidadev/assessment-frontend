import React, { useState } from 'react';
import Navbar from './Navbar';
import SearchBar from './SearchBar';
import { GoSearch } from 'react-icons/go';
import { FiMenu } from 'react-icons/fi';
import logo from '../assets/logo-webjump.png';

const Topbar = () => {
  const [navToggle, setNavToggle] = useState(false);

  const handleNavToggle = () => {
    setNavToggle(!navToggle);
  };

  return (
    <header className="wrapper">
      <section className="wrapper wrapper--top">
        <div className="container container--end">
          <p>
            <a
              href="https://webjump.com.br/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Acesse sua Conta
            </a>
            {' '}ou{' '}
            <a
              href="https://webjump.com.br/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Cadatre-se
            </a>
          </p>
        </div>
      </section>
      <section className="search">
        <div className="container container--row">
          <FiMenu
            className="icon-mobile"
            color="#242020"
            size="3rem"
            onClick={handleNavToggle}
          />
          <figure className="search__img">
            <img src={logo} alt="WebJump" />
          </figure>
          <GoSearch className="icon-mobile" color="#cc0c1f" size="3rem" />
          <SearchBar />
        </div>
      </section>
      <Navbar navToggle={navToggle} handleNavToggle={handleNavToggle} />
    </header>
  );
};

export default Topbar;
