import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { GoX } from 'react-icons/go';
import api from '../services/api';

const Navbar = ({ navToggle, handleNavToggle }) => {
  const [links, setLinks] = useState([]);

  useEffect(() => {
    api
      .get('list')
      .then(response => {
        setLinks(response.data.items);
      })
      .catch(console.error);
  }, []);

  return (
    <section className={`navbar ${navToggle ? 'navbar--active' : ''}`}>
      <div className="container">
        <nav className="navbar__links">
          <GoX
            color="#ffffff"
            size="2rem"
            className="navbar__icon"
            onClick={handleNavToggle}
          />
          <Link onClick={handleNavToggle} to="/" className="navbar__link">
            Página inicial
          </Link>
          {links.map(link => (
            <Link
              onClick={handleNavToggle}
              key={link.id}
              className="navbar__link"
              to={link.path}
            >
              {link.name}
            </Link>
          ))}
          <a
            href="https://webjump.com.br/telefone-da-webjump/"
            target="_blank"
            className="navbar__link"
            rel="noopener noreferrer"
          >
            Contato
          </a>
        </nav>
      </div>
    </section>
  );
};

export default Navbar;
