import React from 'react';
import Routes from './Routes';
import Topbar from './components/Topbar';
import Footer from './components/Footer';

const App = () => (
  <>
    <div className="top">
      <Topbar />
      <Routes />
    </div>
    <Footer />
  </>
);

export default App;
