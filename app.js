require('dotenv').config();
var app = require('connect')()
var serveStatic = require('serve-static')
const cors = require('cors')

// CORS config
app.use(
  cors({
    credentials: true,
    origin: [
      'http://localhost:3000',
      'http://webjump-rickyalmeida.herokuapp.com',
      'https://webjump-rickyalmeida.herokuapp.com'
    ],
  })
);

// Serve up mock-api folder
app.use('/api', serveStatic('mock-api', {
  'index': false,
  'setHeaders': setJsonHeaders
}))
 
// Set header to force download
function setJsonHeaders (res, path) {
  res.setHeader('Content-type', 'application/json')
}

// Serve up public folder
app.use('/', serveStatic('public', {'index': ['index.html', 'index.htm']}))

app.listen(process.env.PORT, function() {
    console.log('Acesse: http://localhost:8888')
});
